<?php

/**
 * Class ApiController
 */
class ApiController extends \BaseController {

    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondNotFound($message = 'Not Found!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondInternalError($message = 'Internal Error!')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }

    /**
     * @param       $data
     * @param array $headers
     *
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param $message
     *
     * @return mixed
     */
    public function respondWithError($message)
    {
        return $this->respond([

            'error' => [
                'message'     => $message,
                'status_code' => $this->getStatusCode()
            ]

        ]);
    }

    public function respondOk($message)
    {
        return $this->respond([

            'success' => [
                'message'     => $message,
                'status_code' => $this->getStatusCode()
            ]

        ]);
    }

    /**
     * @param \Illuminate\Pagination\Paginator $ressources
     * @param array                            $data
     *
     * @return mixed
     */
    public function respondWithPagination(\Illuminate\Pagination\Paginator $ressources, $data)
    {
        $data = array_merge($data, [

            'paginator' => [
                'total_count'  => $ressources->getTotal(),
                'current_page' => $ressources->getCurrentPage(),
                'total_page'   => ceil($ressources->getTotal() / $ressources->getPerPage()),
                'limit'        => $ressources->getPerPage()
            ]

        ]);

        return $this->respond($data);
    }

} 