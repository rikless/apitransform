<?php namespace Api;

use \Woufapp\Repositories\Subscriptions\HistoryRepository;
use \Woufapp\Transformers\SubscriptionTransformer;

class SubscriptionsHistoryController extends \ApiController {

    protected $subscription;
    protected $subscriptionTransformer;

    public function __construct(HistoryRepository $subscription, SubscriptionTransformer $subscriptionTransformer)
    {

        $this->subscription = $subscription;
        $this->subscriptionTransformer = $subscriptionTransformer;
        $this->beforeFilter('Sentinel\inGroup:Users');
    }

    /**
     * Display a listing of the resource.
     * GET /subscriptions
     *
     * @return Response
     */
    public function index()
    {
        if($request = \Request::get('search'))
        {

            $subscriptions = \Search::subscription($request, 5000);

        }else{

            $subscriptions = $this->subscription->paginateDesc(1000, ['*']);

        }


        return $this->respondWithPagination($subscriptions, [

            'data' => $this->subscriptionTransformer->transformCollection($subscriptions->all()),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /subscriptions/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /subscriptions
     *
     * @return Response
     */
    public function store()
    {


    }

    /**
     * Display the specified resource.
     * GET /subscriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subscription = $this->subscription->find($id);

        return View::make('subscriptions.show', compact('subscription'));
    }

    /**
     * Show the form for editing the specified resource.
     * GET /subscriptions/{id}/edit
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /subscriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $subscription = $this->subscription->find($id);

        $input = \Input::all();

        try
        {
            $data = $this->subscriptionTransformer->revertTransform($input);

        }catch ( \InvalidArgumentException $e)
        {
            return $this->setStatusCode(500)->respondWithError($e->getMessage());
        }

        if($data['status'] == 'suspended')
            $data['date_suspension'] = new \DateTime("now");

        $subscription->update($data);

        return $this->respondOk('subscription updated');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /subscriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}