<?php
use \Woufapp\Repositories\Subscriptions\HistoryRepository;

class SubscriptionHistoryController extends \BaseController {


    protected $subscription;

    public function __construct(HistoryRepository $subscription){

        $this->subscription = $subscription;
        $this->beforeFilter('Sentinel\inGroup:Users');
    }

    /**
	 * Display a listing of the resource.
	 * GET /subscriptionshistory
	 *
	 * @return Response
	 */
	public function index()
	{
        if($request = Request::get('search'))
        {
            $subscriptions = Search::subscription($request);
            return View::make('subscriptionshistory.index', compact('subscriptions', 'request'));
        }

        $subscriptions = $this->subscription->orderDesc(2000);

        return View::make('subscriptionshistory.index', compact('subscriptions'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /subscriptionshistory/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /subscriptionshistory
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /subscriptionshistory/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /subscriptionshistory/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$subscription = $this->subscription->find($id);

        return View::make('subscriptionshistory.edit', compact('subscription'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /subscriptionshistory/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $subscription = $this->subscription->find($id);

        $validator = Validator::make($data = Input::all(), $this->subscription->model->changeState);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $subscription->update($data);

        return Redirect::route('subscriptions.history.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /subscriptionshistory/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}