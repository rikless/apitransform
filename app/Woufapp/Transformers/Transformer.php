<?php namespace Woufapp\Transformers;


/**
 * Class Transformer
 * @package Woufapp\Transformers
 */
abstract class Transformer {

    /**
     * @param $items
     * @return array
     */
    public function transformCollection($items)
    {
        return array_map( [$this, 'transform' ], $items );
    }

    public function revertTransform($items)
    {
        $output = [];

        foreach ( $items as $input => $value ) {

            if ( array_key_exists($input, $this->dbFields) ) {

                $dbKey = $this->dbFields[$input];

                $output[$dbKey] =  $value;

            }
            else{

                throw new \InvalidArgumentException('Fileds doesn\'t exists for this ressource');

            }

        }

        return $output;

    }

    /**
     * @param $item
     * @return mixed
     */
    abstract function transform($item);

} 