<?php namespace Woufapp\Transformers;
use Illuminate\Support\Collection;

/**
 * Class CustomerTransformer
 * @package Woufapp\Transformers
 */
class SubscriptionTransformer extends Transformer {

    protected $dbFields = [
        'id'             => 'id_subscription_history',
        'id_order'       => 'id_order',
        'status'         => 'status',
        'id_address'     => 'id_address_delivery',
        'start'          => 'date_from',
        'finish'         => 'date_to',
        'suspension'     => 'date_suspension',
        'end_suspension' => 'date_suspension_fin',
        'shop'           => 'id_shop',

    ];

    /**
     * @param $ressource
     *
     * @return array
     */
    public function transform($ressource)
    {

        return [
            'id'             => $ressource['id_subscription_history'],
            'id_order'       => $ressource['id_order'],
            'status'         => $ressource['status'],
            'id_address'     => $ressource['id_address_delivery'],
            'start'          => $ressource['date_from'],
            'finish'         => $ressource['date_to'],
            'suspension'     => $ressource['date_suspension'],
            'end_suspension' => $ressource['date_suspension_fin'],
            'shop'           => $ressource['id_shop'],
        ];

    }

} 